file1 = open("dev_results.txt")
file2 = open("output_threshold_0_13.txt")

line1 = file1.readlines()
line2 = file2.readlines()

i = 0

#ERROR DETECTION
true_pos = 0	#Correctly detected an error
true_pos_classification = 0
true_neg = 0	#Correctly detected as NOT an error
false_neg = 0	#Detected as NOT an error but IT is an error
false_pos = 0	#Detected as ERROR but it is NOT an error

for i in range(len(line1)):
	if line1[i] != '':

		l1 = line1[i].split(' ')
		l2 = line2[i].split(' ')

		if(len(l1) > 1 and len(l2) > 1):
			true_pos += 1
			if(line1[i] == line2[i]):
				true_pos_classification += 1
		else:
			if(line1[i] == line2[i]):
				true_neg += 1
			else:
				if(len(l1) > 1):
					false_neg += 1
				else:
					false_pos += 1

accuracy = true_pos_classification / float(true_pos + false_neg)

accuracy *= 100

accuracy_detection = (true_pos + true_neg) / float(true_pos + true_neg + false_pos + false_neg)

accuracy_detection *= 100

print("ERRORS detected correctly: " + str(true_pos))
print("ERRORS classified correctly: " + str(true_pos_classification))
print("Correctly detected as NOT an error: " + str(true_neg))
print("Detected as NOT an error but IT is an error: " + str(false_neg))
print("Detected as ERROR but it is NOT an error: " + str(false_pos))

print("Accuracy % in Error Detection: " + str(accuracy_detection))
print("Accuracy % in Error Classification : " + str(accuracy))