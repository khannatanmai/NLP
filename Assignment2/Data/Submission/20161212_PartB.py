from collections import Counter

def tokenise(text):
	#We're going to put a start of sentence $SOS# and end of sentence $EOS# marker in our data
	sentences = [['$SOS#']]
	sentence_count = 0

	for line in text:
		if(line == '\n'):
			sentences[sentence_count].append('$EOS#') #Append EOS when a sentence ends
			sentences.append(['$SOS#'])
			sentence_count += 1
		else:
			word = line.strip('\n')
			sentences[sentence_count].append(word)

	return sentences


def language_model(sentences):
	trigrams = []

	#LM for Grammar Check
	for sentence in sentences:
		count = 0
		while(len(sentence) - count >=3):
			trigrams.append(sentence[count:count+3])
			count = count + 1

	#print(trigrams[0:100])

	trigrams_new = []
	error_trigrams = []

	error_in_classes = []

	for trigram in trigrams:
		trigram_new = []

		error_flag = 0
			
		trigram_new.append(trigram[0].split(' ')[0]) #adding part 1 in new trigram

		middle_word = trigram[1].split(' ')

		if(len(middle_word) > 1):
			error_flag = 1					#if middle word in trigram is an error (has more than one word), add in error trigrams list

		trigram_new.append(middle_word[0]) #Add middle word in trigram

		trigram_new.append(trigram[2].split(' ')[0]) #Add last word in trigram

		trigrams_new.append('~'.join(trigram_new))

		if error_flag != 0:
			error_trigrams.append('~'.join(trigram_new))
			#print(middle_word[-1])
			
			if middle_word[-1] not in Error_Classes: #Adding type of error in list of error classes
				Error_Classes.append(middle_word[-1])
				error_in_classes.append([])

				index_error_class = Error_Classes.index(middle_word[-1])
				error_in_classes[index_error_class].append('~'.join(trigram_new)) #Adding the word in the corresponding error class index
			else:
				index_error_class = Error_Classes.index(middle_word[-1])
				error_in_classes[index_error_class].append('~'.join(trigram_new)) #Adding the word in the corresponding error class index

	#print(trigrams_new[0:100])
	#print(error_trigrams[0:100])
	
	#print(error_in_classes)
	n_grams = [trigrams_new, error_trigrams, error_in_classes]


	return n_grams

	###End of Language Model function

def counts(n_grams):
	all_trigrams = n_grams[0]
	error_trigrams = n_grams[1]
	error_in_classes = n_grams[2]

	unigrams = []

	for trigram in all_trigrams:
		temp_trigram = trigram.split('~')
		unigrams.append(temp_trigram[1])

	unigrams_count = Counter(unigrams)

	error_unigrams = []

	for trigram in error_trigrams:
		temp_trigram = trigram.split('~')
		error_unigrams.append(temp_trigram[1])

	error_unigrams_count = Counter(error_unigrams)

	#vocabulary_size = float(len(Counter(unigrams))) #Getting number of unique words in our data
	#print(vocabulary_size)

	all_trigrams_count = Counter(all_trigrams)
	error_trigrams_count = Counter(error_trigrams)

	error_in_classes_count = []

	#print(all_trigrams_count.most_common(50))

	for error_class in error_in_classes:
		error_class_count = Counter(error_class)
		error_in_classes_count.append(error_class_count) #Each index of this has one class and counts of words in it

	#Getting Error Classes for Unigrams
	
	unigram_error_in_classes = []

	for error_class in error_in_classes:
		unigram_error_class = []
		for trigram in error_class:
			unigram = trigram.split('~')[1]
			unigram_error_class.append(unigram)

		unigram_error_in_classes.append(unigram_error_class)

	unigram_error_in_classes_count = []

	#Now Getting Error Classes Counts for Unigrams
	for error_class in unigram_error_in_classes:
		error_class_count = Counter(error_class)
		unigram_error_in_classes_count.append(error_class_count) #Each index of this has one class and counts of words in it

	#print(error_in_classes_count)
	n_grams_counts = [all_trigrams_count, error_trigrams_count, error_in_classes_count, unigrams_count, error_unigrams_count, unigram_error_in_classes_count]
	
	return n_grams_counts

	###End of Counting Function

def error_probability(input_trigram, n_grams_counts):

	all_trigrams_count = n_grams_counts[0]
	total_trigrams = sum(all_trigrams_count.values())

	error_trigrams_count = n_grams_counts[1]
	total_errors = sum(error_trigrams_count.values())

	unigrams_count = n_grams_counts[3]
	total_unigrams = sum(unigrams_count.values())

	error_unigrams_count = n_grams_counts[4]
	total_unigram_errors = sum(error_unigrams_count.values())

	#Using Naive Bayes' rule, we Need P(Error|Trigram) = P(Trigram|Error) * P(Error) / P(Trigram)

	if(all_trigrams_count[input_trigram] > 0):
		P_TE = error_trigrams_count[input_trigram] / float(total_errors)
		
		P_E = total_errors / float(total_trigrams)
		
		P_T = all_trigrams_count[input_trigram] / float(total_trigrams) 
		
	else: #BackOff to Unigrams
		middle_word = input_trigram.split('~')[1]

		#If the trigram isn't present in our data, we will use P(Word|Error) = Count(Word as an error) / Total Errors
		
		P_TE = error_unigrams_count[middle_word] / float(total_unigram_errors)

		P_E = total_unigram_errors / float(total_unigrams)

		P_T = unigrams_count[middle_word] / float(total_unigrams)

	#print(P_TE)	
	#print(P_E)
	#print(P_T)
	#print(all_trigrams_count[input_trigram])
	#print(total_trigrams)

	if(P_T > 0):
		P_ET = float(P_TE) * float(P_E) / float(P_T)
	else:
		P_ET = float(0) #If we havent seen the word at all in our corpus we cant say anything about it

	#print("final")
	#print(P_ET)
	return P_ET

	### End of Error Probability Function

def error_classification(error_trigram, n_grams_counts):
	max_probability = 0
	max_class_index = -1

	error_trigrams_count = n_grams_counts[1] #This is original list of all errors
	total_errors = sum(error_trigrams_count.values())

	errors_in_classes = n_grams_counts[2] #This is errors in their classes
	unigram_errors_in_classes = n_grams_counts[5]

	error_unigram = error_trigram.split('~')[1]

	i = 0

	for i in range(len(Error_Classes)):
		trigram_counts_in_class = errors_in_classes[i]
		total_errors_in_class = sum(errors_in_classes[i].values())

		unigram_counts_in_class = unigram_errors_in_classes[i]
		total_unigram_errors_in_class = sum(unigram_errors_in_classes[i].values())

		#Using Naive Bayes' rule, we Need max of P(Error_Class|Trigram) = P(Trigram|Error_Class) * P(Error_Class) 
		#We can ignore the denominator as it will stay the same

		if(trigram_counts_in_class[error_trigram] > 0):
			P_TEC = trigram_counts_in_class[error_trigram] / float(total_errors_in_class) 
			P_E = total_errors_in_class / float(total_errors)
		
		else: #Backoff to Unigrams
			P_TEC = unigram_counts_in_class[error_unigram] / float(total_unigram_errors_in_class)
			P_E = total_unigram_errors_in_class / float(total_errors)

		P_ECT = float(P_TEC) * float(P_E)

		if(P_ECT > float(max_probability)):
			#print(i)
			max_probability = float(P_ECT)
			max_class_index = i

	#Now, we have the class index of the class which has the highest probability

	#print(max_probability)
	final_error_class = Error_Classes[max_class_index]
	#print(final_error_class)
	
	return final_error_class

	### End of Error Classification Function



def error_detection(input_sentences, n_grams_counts):
	output_file = open("output_threshold_0_13.txt", "a+")

	trigrams = []

	threshold_for_error = 0.13 #Seen Experimentally

	#LM for Grammar Check
	for sentence in input_sentences:
		count = 0
		while(len(sentence) - count >=3):
			trigrams.append(sentence[count:count+3])
			count = count + 1

		trigrams.append(' ')

	count = 0
	for trigram in trigrams:
		count += 1
		print(count)
		if(trigram == ' '):
			#PRINT SPACE IN THE OUTPUT FILE
			output_file.write('\n')
		else:
			trigram_new = '~'.join(trigram)

			probability = error_probability(trigram_new, n_grams_counts)
			#print(trigram[1])
			#print(probability)

			
			if(probability >= threshold_for_error): #This word is an error
				error_class = error_classification(trigram_new, n_grams_counts)
				
				output_file.write(str(trigram[1]) + '   ' + error_class + '\n') # + str(probability))
			
			else: #This word is NOT an error
				output_file.write(str(trigram[1]) + '\n')
			

	#By the end of this function we should have an output file

	### End of Error Detection and Output Creation Function

#MAIN
Error_Classes = []

file = open("train.txt")
text = file.readlines()

sentences = tokenise(text)

#print(sentences)

n_grams = language_model(sentences)

n_grams_counts = counts(n_grams) #This also contains counts of words in their error classes

input_file = open("dev.txt")
input_text = input_file.readlines()

input_sentences = tokenise(input_text)

#print(input_sentences)
error_detection(input_sentences, n_grams_counts)

#error_probability(',~safxely~of', n_grams_counts)
#error_probability(',~safely~of', n_grams_counts)
#error_probability('will~refuse~to', n_grams_counts)

#print(Error_Classes)


#TO_DO
#9. Decide on a Threshold for error
