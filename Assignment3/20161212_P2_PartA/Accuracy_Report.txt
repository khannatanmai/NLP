THRESHOLD = 0.1:
ERRORS detected correctly: 4623
ERRORS classified correctly: 1236
Correctly detected as NOT an error: 29825
Detected as NOT an error but IT is an error: 2688
Detected as ERROR but it is NOT an error: 19927

Accuracy % in Error Detection: 60.36836478979374
Accuracy % in Error Classification : 16.90603200656545

THRESHOLD = 0.13:
ERRORS detected correctly: 3417
ERRORS classified correctly: 964
Correctly detected as NOT an error: 38051
Detected as NOT an error but IT is an error: 3894
Detected as ERROR but it is NOT an error: 11701

Accuracy % in Error Detection: 72.67055710355221
Accuracy % in Error Classification : 13.185610723567228

THRESHOLD = 0.16:
ERRORS detected correctly: 2567
ERRORS classified correctly: 741
Correctly detected as NOT an error: 42534
Detected as NOT an error but IT is an error: 4744
Detected as ERROR but it is NOT an error: 7218

Accuracy % in Error Detection: 79.03720449327936
Accuracy % in Error Classification : 10.135412392285598

THRESHOLD = 0.20:
ERRORS detected correctly: 2061
ERRORS classified correctly: 596
Correctly detected as NOT an error: 44972
Detected as NOT an error but IT is an error: 5250
Detected as ERROR but it is NOT an error: 4780

Accuracy % in Error Detection: 82.42293605313425
Accuracy % in Error Classification : 8.152099575981397

